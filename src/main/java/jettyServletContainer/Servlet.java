package jettyServletContainer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Use this class for parent to your servlets
 *
 * Override {@link Servlet#GET(HttpServletRequest, Map, HttpServletResponse)},
 * {@link Servlet#POST(HttpServletRequest, Map, HttpServletResponse)},
 * {@link Servlet#PUT(HttpServletRequest, Map, HttpServletResponse)},
 * {@link Servlet#DELETE(HttpServletRequest, Map, HttpServletResponse)}
 * to build your business logic
 *
 * You can override {@link Servlet#beforeAction(HttpServletRequest, Map, HttpServletResponse)}
 * to check authorization or something else before actions
 */
public class Servlet extends HttpServlet {
    @Override
    protected final void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(req.getParameterMap());
        if (beforeAction(req, parsedParameters, resp))
            GET(req, parsedParameters, resp);
        else {
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @Override
    protected final void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(req.getParameterMap());
        if (beforeAction(req, parsedParameters, resp))
            POST(req, parsedParameters, resp);
        else {
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @Override
    protected final void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(req.getParameterMap());
        if (beforeAction(req, parsedParameters, resp))
            PUT(req, parsedParameters, resp);
        else {
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @Override
    protected final void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, Object> parsedParameters = ParseParametersHelper.parse(req.getParameterMap());
        if (beforeAction(req, parsedParameters, resp))
            DELETE(req, parsedParameters, resp);
        else {
            resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    protected void GET(HttpServletRequest request, Map<String, Object> parsedParameters,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }

    protected void POST(HttpServletRequest request, Map<String, Object> parsedParameters,
                        HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }

    protected void PUT(HttpServletRequest request, Map<String, Object> parsedParameters,
                       HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }

    protected void DELETE(HttpServletRequest request, Map<String, Object> parsedParameters,
                          HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }

    protected boolean beforeAction(HttpServletRequest request, Map<String, Object> parsedParameters,
                                HttpServletResponse response) {
        return true;
    }
}
