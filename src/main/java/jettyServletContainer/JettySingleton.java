package jettyServletContainer;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.MultipartConfigElement;
import javax.servlet.http.HttpServlet;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Main class to create Jetty web server in 3 steps
 * Usage:
 * 1) Add servlets
 * <pre>
 *     JettySingleton.getInstance().addServlet(String contextPath, String pathSpec, Servlet servlet)
 * </pre>
 * 2) Set is allow for all domains
 * <pre>
 *     JettySingleton.getInstance().setAvailableForAll(boolean availableForAll)
 * </pre>
 * 3) Run server
 * <pre>
 *     JettySingleton.getInstance().runServer();
 * </pre>
 *
 * This class creates {@link ServletContextHandler} for each unique contextPath automatically
 */
public class JettySingleton {
    public enum CONTENT_TYPE {
        HTML("text/html;charset=utf8"),
        JSON("application/json;charset=utf8");

        private final String contentType;

        CONTENT_TYPE(String contentType) {
            this.contentType = contentType;
        }

        public String getContentType() {
            return contentType;
        }

        @Override
        public String toString() {
            return getContentType();
        }
    }

    private static JettySingleton ourInstance = new JettySingleton();
    private int port = 8080;
    private Map<String, ServletContextHandler> servletContextHandlerMap = new HashMap<>();
    private boolean availableForAll = false;

    public static JettySingleton getInstance() {
        return ourInstance;
    }

    private JettySingleton() {
    }

    public void setPort(int port) {
        if (port >= 1024 && port <= 49151)
            this.port = port;
    }

    public ServletContextHandler getServletContextHandler(String contextPath) {
        if (!servletContextHandlerMap.containsKey(contextPath)) {
            ServletContextHandler handler = new ServletContextHandler();
            handler.setContextPath(contextPath);
            servletContextHandlerMap.put(contextPath, handler);

            return handler;
        }

        return servletContextHandlerMap.get(contextPath);
    }

    public boolean isAvailableForAll() {
        return availableForAll;
    }
    public void setAvailableForAll(boolean availableForAll) {
        this.availableForAll = availableForAll;
    }

    public void addServlet(String contextPath, String servletPath, HttpServlet servlet, boolean availableFileUpload) {
        ServletContextHandler handler = getServletContextHandler(contextPath);
        ServletHolder servletHolder = new ServletHolder(servlet);

        if (availableFileUpload) {
            servletHolder.getRegistration().setMultipartConfig(new MultipartConfigElement("/tmp"));
        }

        handler.addServlet(servletHolder, servletPath);
    }
    public void addServlet(String contextPath, String servletPath, HttpServlet servlet) {
        addServlet(contextPath, servletPath, servlet, false);
    }

    public void runServer() throws Exception {
        Server server = new Server(port);

        ContextHandlerCollection contextHandlerCollection = new ContextHandlerCollection();

        if (isAvailableForAll()) {

            servletContextHandlerMap.values().forEach(servletContextHandler -> {
                FilterHolder cors = servletContextHandler.addFilter(CrossOriginFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));

                cors.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
                cors.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
                cors.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,POST,HEAD,PUT,DELETE,OPTIONS");
                cors.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "X-Requested-With,Content-Type,Accept,Origin,Session");
            });
        }

        contextHandlerCollection.setHandlers(servletContextHandlerMap.values().toArray(new Handler[]{}));

        server.setHandler(contextHandlerCollection);
        server.start();
        server.join();
    }
}
